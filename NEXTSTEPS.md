This file contains some code that we came up with while working towards our next step, which was to query the API for all parcels in the current viewport of the map and display them accordingly.


This is the function intended to query the API - it would go in the api.service.ts file.
  
getMapParcels(coordinates) {
    console.log(`${this.apiUrl}/parcels/${coordinates}`)
    return this.http.get(`${this.apiUrl}/parcels/${coordinates}`).then((res) => {
    console.log(res.response);
    return res.response;
    });
}


This portion of code was part of the Map.tsx file - it lived inside the map.current.on('load', function () {
The goal here is to find the four corners of the map, and send them to the API as a bounding box which we can then retrieve all parcels contained inside the given latitude and longitudes
    
    let bounds = map.current.getBounds();

    let coordinates = `${bounds._sw.lng} ${bounds._sw.lat},
      ${bounds._sw.lng} ${bounds._ne.lat},
      ${bounds._ne.lng} ${bounds._ne.lat},
      ${bounds._ne.lng} ${bounds._sw.lat},
      ${bounds._sw.lng} ${bounds._sw.lat}`
    
    
    This would call the getMapParcels funtion from the api.service file and send the coordinates to the api, with the coordinates as a path. Then, the api would take the coordinates from the path and plug them into a function that would do query the database.

ApiService.getMapParcels(coordinates)
    .then((res) => parcels = res);


The initial plan was to use the current Parcels module to query the API using something like the following code:

Parcel.getMapParcels = async (coordinates) => {
        const mapParcels = await sequelize.query(`SELECT allegheny_county_parcel_boundaries.wkb_geometry 
        FROM allegheny_county_parcel_boundaries 
        WHERE ST_CONTAINS(ST_GeomFromText("POLYGON${coordinates}, 4326"), allegheny_county_parcel_boundaries.wkb_geometry)`, { type: QueryTypes.SELECT })

        return mapParcels;
      };

Unfortunately, an issue arose while querying this database. This specific query worked when hardcoding the coordinates and using SQL scripts in DBeaver. However, even when hardcoding coordinates into the API query (testing before trying to use the coordinates from the app), all that was being returned was an empty array.
