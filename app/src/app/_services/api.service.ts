import HttpService from './http.service';
import ConfigService  from './config.service';

class ApiService {

  public apiUrl: string;
  private http: typeof HttpService = HttpService;
  private config: typeof ConfigService = ConfigService;

  constructor() {
    const apiUrl = this.config.get('API_URL');
    this.apiUrl = apiUrl ?? '';
  }

  getStatus() {
    return this.http.get(`${this.apiUrl}/`).then((res) => {
      return res;
    });
  }
  
  getMapParcels(coordinates) {
    return this.http.get(`${this.apiUrl}/mapparcels/${coordinates}`).then((res) => {
      return res.response;
    });
  }
}

export default new ApiService();
