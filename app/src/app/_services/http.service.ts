export class HttpService {

  request(type: string, endpoint: string, data?: any): Promise<any> {
    return new Promise((resolve, reject) => {
      const xhr = new XMLHttpRequest();

      xhr.open(type, endpoint);

      xhr.onreadystatechange = (() => {
        if (xhr.readyState !== 4) return;

        if (xhr.status !== 0 && (xhr.status < 200 || xhr.status >= 400)) {
          reject({
            status: xhr.status, 
            response: xhr.responseText
          });
          return;
        }

        // NOTE: Probably also an error? Seen when:
        // - Server isn't running/Cors Failure
        if (xhr.status === 0) {
          resolve({
            status: xhr.status, 
            response: xhr.responseText
          });
          return;
        }
        
        resolve({
          status: xhr.status, 
          response: JSON.parse(xhr.responseText)
        });
        return;
      });
      
      xhr.send(data);

    });
  }
  
  get(endpoint: string): Promise<any> {
    return this.request('GET', endpoint);
  }

  post(endpoint: string, data: any): Promise<any> {
    return this.request('POST', endpoint, data);
  }

  patch(endpoint: string, data: any): Promise<any> {
    return this.request('PATCH', endpoint, data);
  }

  put(endpoint: string, data: any): Promise<any> {
    return this.request('PUT', endpoint, data);
  }

  delete(endpoint: string): Promise<any> {
    return this.request('DELETE', endpoint);
  }
}

export default new HttpService();
