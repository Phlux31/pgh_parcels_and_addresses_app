import React from 'react';
import { render, screen } from '@testing-library/react';
import { Map } from './Map';

test('renders Map', () => {
  const map = render(<Map />);
  expect(map).not.toThrowError();
});
