import { useState, useRef, useEffect } from 'react';
import ConfigService from '../../_services/config.service';
import ApiService from '../../_services/api.service'
import './Map.css';
import mapboxgl from 'mapbox-gl';

export function Map() {
  mapboxgl.accessToken = ConfigService.get('MAP_TOKEN');

  // the variables and two useEffect()s make the map displayable, zoomable, and movable
  const mapContainer = useRef<HTMLDivElement>(null);
  const map = useRef(null);
  const [lng, setLng] = useState(-80.0196);
  const [lat, setLat] = useState(40.3931);
  const [zoom, setZoom] = useState(18.1);

  useEffect(() => {
    if (map.current) return; // initialize map only once
    map.current = new mapboxgl.Map({
      container: mapContainer.current,
      style: 'mapbox://styles/mapbox/streets-v11',
      center: [lng, lat],
      zoom: zoom
    });
  });

  useEffect(() => {
    if (!map.current) return; // wait for map to initialize
    map.current.on('move', () => {
      setLng(map.current.getCenter().lng.toFixed(4));
      setLat(map.current.getCenter().lat.toFixed(4));
      setZoom(map.current.getZoom().toFixed(2));
    });

    function loadParcels() {
      let bounds = map.current.getBounds();  // gets location data of SW and NE corners of map

      let coordinates = `${bounds._sw.lng}_${bounds._sw.lat},${bounds._sw.lng}_${bounds._ne.lat},${bounds._ne.lng}_${bounds._ne.lat},${bounds._ne.lng}_${bounds._sw.lat},${bounds._sw.lng}_${bounds._sw.lat}`;  // coordinates of corners of box, which draw a polygon for ST_CoveredBy when sent to API
      // TODO: potentially reduce to just two coordinate pairs, although we are not sure if ST_CoveredBy can work with only two coordinate pairs, so if we reduce to two, we may need to change the API query

      let sourceData = [];

      // this function accesses the api, had to wrap all below code inside the .then() in order to access the data returned from api
      ApiService.getMapParcels(coordinates).then(function (parcels) {
        // iterates each item in the array to create multiple features which we will use to add location data to the map as a source
        for (let i = 0; i < parcels.length; i++) {
          sourceData.push({
            'type': 'Feature',
            'properties': {  // couldn't just add 'pin' here - 'pin' had to be within 'properties' to be able to access it from the on-click function below
              'pin': parcels[i].pin
            },
            'geometry': {
              'type': 'Polygon',
              'coordinates': parcels[i].geometry.coordinates
            }
          })
        }

        // these if statements remove any current layers and sources if they exist so that we don't get an error
        if (map.current.getLayer(`parcel-shading`)) {
          map.current.removeLayer(`parcel-shading`);
        }
        if (map.current.getLayer(`parcel-outline`)) {
          map.current.removeLayer(`parcel-outline`);
        }
        if (map.current.getSource(`parcels`)) {
          map.current.removeSource(`parcels`);
        }
        // adds data source of geojson location data to map containing all of the parcels returned from the api query
        map.current.addSource(`parcels`, {
          "type": "geojson",
          "data": {
            'type': 'FeatureCollection',
            'features': sourceData
          }
        });
        // adds a layer of shading to visualize the parcel
        map.current.addLayer({
          'id': `parcel-shading`,
          'type': 'fill',
          'source': `parcels`, //references the data source
          'layout': {},
          'paint': {
            'fill-color': '#ff69b4',
            'fill-opacity': 0.5
          }
        });
        // adds a black outline around parcel
        map.current.addLayer({
          'id': `parcel-outline`,
          'type': 'line',
          'source': `parcels`,
          'layout': {},
          'paint': {
            'line-color': '#000',
            'line-width': 3
          }
        });
      });
    }

    map.current.on('load', () => {
      const waiting = () => {
        if (!map.current.isStyleLoaded()) {
          setTimeout(waiting, 200);
        } else {  // currently this is how we have gotten the parcels to display on map load and on map move
          loadParcels();
          map.current.on('moveend', () => {
            loadParcels();
          })
        }
      };
      waiting();
    });

    // When a click event occurs on a feature in the parcels layer, open a popup at the location of the click, with PIN from its properties.
    map.current.on('click', 'parcel-shading', function (e) {
      new mapboxgl.Popup()
        .setLngLat(e.lngLat)
        .setHTML(e.features[0].properties.pin)
        .addTo(map.current);
    });

    map.current.on('mouseenter', 'parcel-shading', function () {  // Change the cursor to a pointer when the mouse is over the parcels layer
      map.current.getCanvas().style.cursor = 'pointer';
    });

    map.current.on('mouseleave', 'parcel-shading', function () {  // Change it back to a pointer when it leaves
      map.current.getCanvas().style.cursor = '';
    });

    // The code below will add a circle to visualize the address on the map once there is address data from the api
    //   map.current.addLayer({
    //     'id': `address-point`,
    //     'type': 'circle',
    //     'source': `addresses`, 
    //     'layout': {},
    //     'paint': {
    //       'circle-radius': 5,
    //       'circle-color': '#B42222'
    //     }
    //   });
    // }

  });
  return (
    <div>
      <div className="map-infobox">
        Latitude: {lat} | Longitude: {lng} | Zoom: {zoom}
      </div>
      <div ref={mapContainer} className="map-container">
      </div>
    </div>
  );
}
